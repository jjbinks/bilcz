#include "eigen_solver.h"

void eigen_solver(
        int n, double * a,
        double * val_p,
        double * vec_p) {
    int i;
    double * val_p_i = calloc(n, sizeof(double));
    double check_sum = 0;

    LAPACKE_dgeev(LAPACK_ROW_MAJOR, 'N', 'V', n, a, n,
                  val_p, val_p_i, NULL, n, vec_p, n);

    for (i=0; i<n; i++) {
        check_sum += val_p_i[i] > 0 ? val_p_i[i] : -val_p_i[i];
    }

    if (check_sum != 0) {
        printf("Catastrophe on a pas le droit de tricher.."
               "i c'est imaginaire ...: check_sum is %f\n", check_sum);
    }

    free(val_p_i); // well that is a cunning trick sir....
    return;
}

int * sort_value(double complex* val_p, int m){
      int * sorted_pos = (int*) malloc(sizeof(int)*m);
      double *norms = (double*) malloc(sizeof(double)*m);

    int i;
    for (i=0; i<m; i++) {
        sorted_pos[i]=i;
        norms[i]= val_p[i]*conj(val_p[i]);
    }
    //sort
    double swap;
    int swap_p;
    int c,d;
    for (c = 0 ; c < ( m - 1 ); c++)
    {
        for (d = 0 ; d < m - c - 1; d++)
        {
            if (norms[d] < norms[d+1]) /* For decreasing order use < */
            {
               swap       = norms[d];
               norms[d]   = norms[d+1];
               norms[d+1] = swap;

               swap_p          = sorted_pos[d];
               sorted_pos[d]   = sorted_pos[d+1];
               sorted_pos[d+1] = swap_p;

            }
        }
    }
    free(norms);
    return sorted_pos;
}

void eigen_solver_complex(
        int n, double complex * a,
        double complex * val_p,
        double complex * vec_p,
        int nb_eval) {

    double complex *all_val_p = malloc(n*sizeof(double complex));
    double complex *all_vec_p = malloc(n*n*sizeof(double complex));

    LAPACKE_zgeev(LAPACK_ROW_MAJOR, 'N', 'V', n, a, n,
                  all_val_p, NULL, n, all_vec_p, n);

    /*choose biggest vp*/
    int *sorted_pos = sort_value(all_val_p, n);
    int i,j;
    for (i=0; i< nb_eval; i++){
        val_p[i] = all_val_p[sorted_pos[i]];
    }
    for (i=0; i<n; i++){
        for(j=0; j<nb_eval; j++){
            vec_p[i*nb_eval + j] = all_vec_p[i*n + sorted_pos[j]];
        }
    }
    /*or choose smaller error*/
    free(sorted_pos);
    free(all_vec_p);
    free(all_val_p);
}

