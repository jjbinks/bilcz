#include "leia.h"

/* MOVED RESPONSIBILITY TO MAKEFILE */
//#define MAX_DEPTH 100

int iteration = 0;

int get_new_start_vec(
        BLCZ_Problem * problem,
        BLCZ_Result * global_result,
        BLCZ_Result * local_result,
        float * log,
        int worker_id,
        int * workers_state)
{
    int n, nb_eval, flag;
    n = problem->n;
    nb_eval = problem->nb_eval;
    log[iteration] = local_result->error;
    printf("It:%d Global origin : %d\n"
           "Getting result-> result origin:%d from %d worker\n",
           iteration, global_result->origin,
           local_result->origin, worker_id);
    flag = 0;
    if (global_result->error < problem->acceptable_error)
    {
        printf("found acceptable result -- Killing worker\n");
        return -1; /* kill worker */
    }
    if (local_result->error < global_result->error)
    {
        memcpy(global_result->eigenvalues,
               local_result->eigenvalues,
               nb_eval*sizeof(double complex));
        memcpy(global_result->eigenvectors,
               local_result->eigenvectors,
               nb_eval*n*sizeof(double complex));
        global_result->error = local_result->error;
        local_result->origin = ++global_result->origin;
        printf("!!!!!!!!!!!!!!!!!!! getting better: origin=%d\n",
                global_result->origin);
        workers_state[worker_id] = 0;
    }
    else
    {
        if (global_result->origin != local_result->origin)
        {
            /* we pass vector only if it has not already been passed */
            printf("passing better vec to worker\n");
            memcpy(local_result->eigenvalues,
                   global_result->eigenvalues,
                   nb_eval*sizeof(double complex));
            memcpy(local_result->eigenvectors,
                   global_result->eigenvectors,
                   nb_eval*n*sizeof(double complex));
            local_result->error = global_result->error;
            local_result->origin = global_result->origin;
            workers_state[worker_id] = 0;
        }
        else
        {
            #ifndef NDEBUG
                printf("increase step\n");
            #endif
            workers_state[worker_id]++;
        }
    }
    iteration++;
    if (workers_state[worker_id] > MAX_DEPTH+1)
    /* added +1 because test is after increment */
    {
        #ifndef NDEBUG
            printf("PICK NEW START\n");
        #endif
        workers_state[worker_id] = 0;
        flag = 1; /* tell worker to pick new random start */
    }
    return flag;
}

void Leia(BLCZ_Problem * problem,
          BLCZ_Result_Init * result_init,
          BLCZ_Result_Finalize * result_finalize,
          int nworkers)
{
    int i;
    int workers_remaining = nworkers;
    float * log = malloc(nworkers*problem->maxiter*sizeof(float));
    int * workers_state = calloc(nworkers, sizeof(int));
    /* used to control max_depth exploration */
    #ifndef _NO_MPI_
        MPI_Status status;
        int message_size;
    #endif
    double * message;
    int flag;
    int iteration = 0;
    int worker_stopping = 0;
    /* to check if worker has done all its iterations */
    #ifndef _NO_MPI_
        message_size =
    #endif
    message_init(&message, problem);
    BLCZ_Result local_result, global_result;
    result_init(problem, &local_result);
    result_init(problem, &global_result);

    for (i=0; i<nworkers*problem->maxiter; i++)
    {
        if (workers_remaining == 0)
        {
            break; /* no more workers */
        }
        #ifndef _NO_MPI_
            /* receive message from any source */
            #ifndef NDEBUG
                printf("Waiting to receive\n");
            #endif
            MPI_Recv(message, message_size, MPI_DOUBLE, MPI_ANY_SOURCE,
                    0, MPI_COMM_WORLD, &status);
        #endif
        flag = message2result(&local_result, message, problem);
        if (flag == -1)
        {
           worker_stopping = 1;
           /* this was the last iteration of worker */
        }
        #ifndef _NO_MPI_
            flag = get_new_start_vec(
                    problem, &global_result, &local_result,
                    log, status.MPI_SOURCE-1, workers_state);
        #else
            flag = get_new_start_vec(
                    problem, &global_result, &local_result,
                    log, 0, workers_state);
        #endif
        if (flag == -1
         || worker_stopping)
        {
            workers_remaining--; /* killing worker */
            worker_stopping = 0;
        }
        result2message(message, &local_result, problem, flag);
        /* send reply back to sender of the message received above */
        #ifndef _NO_MPI_
            #ifndef NDEBUG
                printf("Sending\n");
            #endif
            MPI_Send(message, message_size, MPI_DOUBLE, status.MPI_SOURCE,
                     0, MPI_COMM_WORLD);
        #endif
        iteration++;
    }

    message_finalize(message);

    print_vect_complex(
            global_result.eigenvalues, problem->nb_eval,
            1, "EIGENVALUES:");

    printf("Error: %.10lg\n", global_result.error);
    FILE * log_file = fopen("log.out", "w");

    if (workers_remaining != 0)
    {
        iteration = nworkers * problem->maxiter;
    }
    for (i=0; i<iteration; i++)
    {
        fprintf(log_file, "%d %f\n", i, log[i]);
    }

    fclose(log_file);
    free(log);

    result_finalize(&local_result);
    result_finalize(&global_result);
}

