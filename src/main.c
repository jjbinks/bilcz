#include "matrix_io.h"  /* IT IS IMPORTANT TO include it above*/
#include <stdlib.h> /* malloc, free, srand */
#include <time.h> /* time */
#include <complex.h> /* double complex */
#include <string.h> /* memcpy */
#include "machtpolitik.h"
#include "print_vect.h"
#include "blcz_method.h"
#include "bilcz.h"
#include "frobenius.h"
#include "leia.h"
#include <omp.h>
#ifndef _NO_MPI_
    #include <mpi.h>
#endif

/* COMPUTED FROM .MTX INSTEAD */
//#define S 8 /* problem space dimension */
/* MOVED RESPONSABILITY TO MAKEFILE */

#ifndef KS
    #define KS 25 /* Krylov subspace dimension */
#endif
#ifndef NV
    #define NV 5  /* Number of eigenvalues */
#endif
#ifndef MAXIT
    #define MAXIT 1000
#endif
#ifndef ACCEPTABLE_ERROR
    #define ACCEPTABLE_ERROR 1e-6
#endif

/*
double complex A[S*S] = {
         1., -1.,  0.,  0.,  0.,  0.,  0.,  0.,
        -1.,  2.,  1.,  0.,  0.,  0.,  0.,  0.,
         0.,  1.,  2.,  1.,  0.,  0.,  0.,  0.,
         0.,  0.,  1.,  2., -1.,  0.,  0.,  0.,
         0.,  0.,  0., -1.,  2.,  1.,  0.,  0.,
         0.,  0.,  0.,  0.,  1.,  2., -1.,  0.,
         0.,  0.,  0.,  0.,  0., -1.,  2.,  1.,
         0.,  0.,  0.,  0.,  0.,  0.,  1.,  1.};

double complex A[S*S] = {
         1., -1.,  0.,  0.,  0.,  0.,  0.,  0.,
         1.,  2.,  1.,  0.,  0.,  0.,  0.,  0.,
         0., -1.,  2., -1.,  0.,  0.,  0.,  0.,
         0.,  0.,  1.,  2.,  1.,  0.,  0.,  0.,
         0.,  0.,  0., -1.,  2., -1.,  0.,  0.,
         0.,  0.,  0.,  0.,  1.,  2.,  1.,  0.,
         0.,  0.,  0.,  0.,  0., -1.,  2., -1.,
         0.,  0.,  0.,  0.,  0.,  0.,  1.,  1.};
*/

MachtPolitik_Worker_Init my_worker_init;
void my_worker_init(
        MachtPolitik_Worker * worker,
        int worker_index)
{
    worker->index               = worker_index;
    worker->problem_init        = my_problem_init;
    worker->input_init          = my_input_init;
    worker->result_init         = my_result_init;
    worker->method              = bilcz;
    worker->result2input        = my_result2input;
    worker->input_finalize      = my_input_finalize;
    worker->problem_finalize    = my_problem_finalize;
    worker->result_finalize     = my_result_finalize;
    worker->inner_data_init     = my_inner_data_init;
    worker->inner_data_finalize = my_inner_data_finalize;
    worker->random_start        = my_random_start;
}

int main(int argc, char * argv[]) {
    /* INIT */
    int rank, size;
    #ifndef _NO_MPI_
        MPI_Init(&argc, &argv);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        if (size < 2) {
            printf("ERROR: A minimum of two process is needed\n");
            MPI_Abort(MPI_COMM_WORLD, 1);;
        }
        if (argc != 2) {
            if (!rank) {
                fputs("usage: bilcz MATRIX.mtx\n", stderr);
            }
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
    #else
        #ifndef NDEBUG
            puts("\033[31m_NO_MPI_\033[m");
        #endif
        rank = 1;
        size = 2;
    #endif

    srand(time(NULL) + 42*rank);
    //srand(666);
    /* EXEC */

    double complex * A;
    int S;
    double A_frob;
    A_frob = get_matrix_from_file(argv[1], &A, &S);
    //A_frob = get_matrix_from_file("matrix/af23560.mtx", &A, &S);

    /*
    double A_frob = frobenius_complex(A, S, S);
    */
    BLCZ_Problem problem;
    my_problem_init(
            &problem, A, A_frob, S, KS + rank*2,
            NV, MAXIT, ACCEPTABLE_ERROR);
    //my_problem_init(
    //        &problem, A, A_frob, S, 100,
    //        NV, MAXIT, ACCEPTABLE_ERROR);

    if (!rank) {
        #ifndef NDEBUG
            printf("I am the boss of %d process!\n", size);
        #endif
        Leia(&problem, my_result_init, my_result_finalize, size-1);
    } else {
        MachtPolitik_Exec(rank, my_worker_init, &problem);
    }
    /* FINALIZE */
    my_problem_finalize(&problem);

    /* if matrix market import free A */
    free(A);
    #ifndef _NO_MPI_
        MPI_Finalize();
    #endif
    return 0;
}

