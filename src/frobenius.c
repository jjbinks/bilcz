#include "frobenius.h"

double frobenius(double * V, int n, int m) {
    double sum;
    int i;
    sum = 0;
    for (i=0; i<n*m; i++) {
        sum += V[i]*V[i];
    }
    return sum;
}

double frobenius_complex(double complex* V, int n, int m) {
    double sum;
    int i;
    sum = 0;
    for (i=0; i<n*m; i++) {
        sum += V[i]*conj(V[i]);
    }
    return sqrt(sum);
}

