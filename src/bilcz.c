#include "bilcz.h"

void bilcz(BLCZ_Problem * problem,
           BLCZ_Input * input,
           BLCZ_Result * result,
           BLCZ_Inner_Data * data) {
    /* INPUT DATA TO COPY FROM problem */
    double complex * A;
    int n, m, nb_eval;

    /* INPUT DATA TO COPY FROM input */
    double complex * v;
    double complex * w;

    /* OUTPUT DATA TO PACK IN result */
    double complex * val_p;
    double complex * Zp; //reprojected eig_vec
    double error;

    /* INNER DATA */
    int i, j;
    double complex * vec_p;
    double complex * T;
    double complex * V;
    double complex * W;
    double complex * ERR;
    double complex * diag_val_p;

    /* PROBLEM DATA COPY */
    A = problem->A;
    n = problem->n;
    m = problem->m;
    nb_eval = problem->nb_eval;

    /* INPUT DATA COPY */
    v = input->v;
    w = input->w;

    //print_vect_complex(v, 2, 1, "Input V:");
    /* inner data copy */
    T = data->T;
    V = data->V;
    W = data->W;
    vec_p = data->vec_p;
    ERR = data->ERR;
    diag_val_p = data->diag_val_p;

    //double a_frob_norm = frobenius_complex(A, n, n);
    Zp = result->eigenvectors;
    val_p = result->eigenvalues;

    //set T to zero
    //USELESS T off diag untouched and T calloc
    //matrix_set_to_zero(T, m, m);

    /* PROJECTION */
    double proj_error;
    proj_error = bilcz_proj(n, A, v, w, m, T, V, W);
    proj_error /= problem->A_frob;
    //print_vect_complex(T, m, m, "Projection res:");

    eigen_solver_complex(m, T, val_p, vec_p, problem->nb_eval);

    for (i=0; i<nb_eval; i++)
    {
        diag_val_p[i*(nb_eval+1)] = val_p[i];
    }

    //print_vect_complex(val_p, m, 1, "val_p:");
    //print_vect_complex(vec_p, m, m, "vect_p:");

    /* TRANS V */
    for (i=0; i<m; i++)
    {
        for (j=0; j<n; j++)
        {
            ERR[j*m+i] = V[i*n+j];
            //ERR[j*m+i] = conj(V[i*n+j]);
        }
    }
    cblas_zcopy(n*m, ERR, 1, V, 1);

    double complex one, zero, minus_one ;
    one  = 1.;
    zero = 0.;
    minus_one = -1.;
    /* REPROJECTION */
    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                n, nb_eval, m, &one, V,
                m, vec_p, nb_eval,
                &zero, Zp, nb_eval);

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                n, nb_eval, n, &one, A,
                n, Zp, nb_eval,
                &zero, ERR, nb_eval);

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                n, nb_eval, nb_eval, &one, Zp,
                nb_eval, diag_val_p, nb_eval,
                &minus_one, ERR, nb_eval);

    //print_vect_complex(ERR, n, m, "ERR:");

    error = frobenius_complex(ERR, n, nb_eval)
          / (nb_eval*problem->A_frob);

    printf("APPROX ERROR: %lg\nREAL ERROR: %lg\n",proj_error,  error);
    result->error = error;

}

