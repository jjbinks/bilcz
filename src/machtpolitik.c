#include "machtpolitik.h"

void MachtPolitik_Result_Update(
        BLCZ_Problem * problem,
        BLCZ_Result * local_result,
        double * message,
        int message_size,
        int * flag)
{
    /* MPI exchange */
    result2message(message, local_result, problem, *flag);
    /* send reply back to sender of the message
     * received above */
    #ifndef _NO_MPI_
        MPI_Status status;
        MPI_Send(message, message_size, MPI_DOUBLE, 0,
                 0, MPI_COMM_WORLD);
        MPI_Recv(message, message_size, MPI_DOUBLE, 0,
                 0, MPI_COMM_WORLD, &status);
    #endif
    *flag = message2result(local_result, message, problem);
}

void MachtPolitik_Exec(
        int id,
        MachtPolitik_Worker_Init * worker_init,
        BLCZ_Problem * problem)
{
    MachtPolitik_Worker worker;
    BLCZ_Input          input;
    BLCZ_Result         result;
    BLCZ_Inner_Data     inner_data;
    int i;
    worker_init(&worker, id);
    worker.result_init(problem, &result);
    worker.input_init(problem, &input);
    worker.inner_data_init(problem, &inner_data);

    double * message;
    int message_size = message_init(&message, problem);
    int flag = 0;
    for (i=0; i<problem->maxiter; i++) {
        worker.method(problem, &input, &result, &inner_data);
        if (i == problem->maxiter-1) {
            flag = -1; /* stopping at next iter */
        }
        MachtPolitik_Result_Update(
                problem, &result,
                message, message_size, &flag);
        if (flag == -1) {
            #ifndef NDEBUG
                printf("I was killed\n");
            #endif
            break; /* Killed by leia */
        } else if (flag == 1) {
            /* worker.random_start(problem, &input);
            weird behaviour: I was expecting random_start
            to be enough to emulate a full restart
            but it is not the case, silent bug in bilcz??*/
            int origin = result.origin;
            worker.input_finalize(&input);
            worker.result_finalize(&result);
            worker.inner_data_finalize(&inner_data);
            worker.result_init(problem, &result);
            worker.input_init(problem, &input);
            worker.inner_data_init(problem, &inner_data);
            result.origin = origin;
        } else {
            /* Leia gave new vectors to work with */
            #ifndef NDEBUG
                printf("Yes mistresssss\n");
            #endif
            worker.result2input(problem, &result, &input);
        }
    }

    message_finalize(message);
    worker.input_finalize(&input);
    worker.result_finalize(&result);
    worker.inner_data_finalize(&inner_data);
}

