#include "print_vect.h"

/*
void print_with_style(double d) {
        if (d <  0.1
         && d > -0.1) {
            printf("%+1.1f",d);
        } else {
            printf("\033[7m%+1.1f\033[0m", d);
        }
}
*/
void print_with_style(double d) {
        if (d <  0.1
         && d > -0.1) {
            printf("%+f",d);
        } else {
            printf("\033[7m%+f\033[0m", d);
        }
}
void print_vect(double * v, int m, int n, char * msg) {
    int i, j;
    puts(msg);
    for (i=0; i<m; i++) {
        for (j=0; j<n; j++) {
            print_with_style(v[i*n+j]);
        }
        putchar('\n');
    }
    putchar('\n');
}

void print_vect_complex(double complex * v, int m, int n, char * msg) {
    int i, j;
    puts(msg);
    for (i=0; i<m; i++) {
        for (j=0; j<n; j++) {
            print_with_style(creal(v[i*n+j]));
            print_with_style(cimag(v[i*n+j]));
            printf("i ");
        }
        putchar('\n');
    }
    putchar('\n');
}

