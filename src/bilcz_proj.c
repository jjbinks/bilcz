#include "bilcz_proj.h"
#include <strings.h>

double bilcz_proj(
        int n,
        double complex * A,
        double complex * v,
        double complex * w,
        int m,
        double complex * T,
        double complex * V,
        double complex * W)
{
    int j, jj, k;
    double complex * a, * b, * d, * t;
    a = malloc(m * sizeof(double complex));
    b = malloc(m * sizeof(double complex));
    d = malloc(m * sizeof(double complex));
    t = malloc(n * sizeof(double complex));
    //char msg[500];
//    b[0] = 1.;
//    d[0] = 1.;
    double complex alpha;
    /* set projected mat to zeros*/
    //for (i=0; i<m*m; i++) proj[i] = 0;

    cblas_zcopy(n, v, 1, V, 1);
    cblas_zcopy(n, w, 1, W, 1); /* CHOICE : w_0 = v_0 */

    for (j=0; j<m-1; j++)
    {
        /* v_j+1 = A * v_j */
        bzero(V+n*(j+1), n*sizeof(double complex));
        #pragma omp parallel for private(k) schedule(static)
        for (jj=0; jj<n; jj++)
        {
            for (k=0; k<n; k++)
            {
                V[n*(j+1)+jj] += A[jj*n+k] * V[j*n+k];
            }
        }

        /* a_j = (A*v_j-1, w_j)*/
        cblas_zdotc_sub(n, V + n*(j+1), 1, W + n*j, 1, a + j);

        if (j)
        {
            /* v_j+1 -= b_j * v_j-1 */
            alpha = -b[j];
            cblas_zaxpy(n, &alpha, V + (j-1)*n, 1, V + n*(j+1), 1);
        }

        /* v_j+1 -= a_j * v_j*/
        alpha = -a[j];
        cblas_zaxpy(n, &alpha, V + j*n, 1, V + (j+1)*n, 1);

        /* W_j+1 = t(A)*W_j - d_j*W_j-1 - a_j*W_j */
        bzero(W + (j+1)*n, n*sizeof(double complex));
        #pragma omp parallel for private(k) schedule(static)
        for (jj=0; jj<n; jj++)
        {
            for (k=0; k<n; k++)
            {
                W[(j+1)*n+jj] += conj(A[k*n+jj]) * W[j*n+k];
            }
        }
        if (j)
        {
            alpha = -d[j];
            cblas_zaxpy(n, &alpha, W + (j-1)*n, 1, W + (j+1)*n, 1);
        }
        alpha = -a[j];
        cblas_zaxpy(n, &alpha, W + j*n, 1, W + (j+1)*n, 1);

        /* d_j+1 = | (v_j+1, w_j+1) |^1/2 */
        /* b_j+1 = (v_j+1, w_j+1) / d_j+1 */
        cblas_zdotc_sub(n, V + n*(j+1), 1, W + n*(j+1), 1, b +j+1);
        d[j+1] = sqrt(creal(b[j+1])*creal(b[j+1])
                    + cimag(b[j+1])*cimag(b[j+1]));
        d[j+1] = sqrt(d[j+1]);
        b[j+1] /= d[j+1];

        /* v_j+1 = v_j+1/d_j+1 */
        alpha = 1. / d[j+1];
        cblas_zscal(n, &alpha, V + (j+1)*n, 1);

        /* w_j+1 = w_j+1/b_j+1 */
        alpha = 1. / b[j+1];
        cblas_zscal(n, &alpha, W + (j+1)*n, 1);
    }

    /* t = A * v_m-1 */
    bzero(t, n*sizeof(double complex));
    #pragma omp parallel for private(k, m, t) schedule(static)
    for (j=0; j<n; j++)
    {
        for (k=0; k<n; k++)
        {
            t[j] += A[j*n+k] * V[(m-1)*n+k];
        }
    }

    /* a_j = (A*v_j, w_j)*/
    cblas_zdotc_sub(n, t, 1, W + n*(m-1), 1, a + m -1);

        /* t -= b_m-1 * v_m-2 */
        alpha = -b[m-1];
        cblas_zaxpy(n, &alpha, V + (m-2)*n, 1, t, 1);

        /* t -= a_m-1 * v_m-1*/
        alpha = -a[m-1];
        cblas_zaxpy(n, &alpha, V + (m-1)*n, 1, t, 1);

        /* W_j+1 = t(A)*W_j - d_j*W_j-1 - a_j*W_j */
        double complex *wm = calloc(n , sizeof(double complex));
        #pragma omp parallel for private(k) schedule(static)
        for (jj=0; jj<n; jj++)
        {
            for (k=0; k<n; k++)
            {
                wm[jj] += conj(A[k*n+jj]) * W[(m-1)*n+k];
            }
        }
        alpha = -d[m-1];
        cblas_zaxpy(n, &alpha, W + (m-2)*n, 1, wm, 1);
        alpha = -a[j];
        cblas_zaxpy(n, &alpha, W + (m-1)*n, 1, wm, 1);
        /* d_m = | (t, wm) |^1/2 */
        /* b_m = (t, wm) / d_m */
        cblas_zdotc_sub(n, t, 1, wm, 1, b);
        d[0] = sqrt(creal(b[0])*creal(b[0])
                    + cimag(b[0])*cimag(b[0]));
        d[0] = sqrt(d[0]);
        b[0] /= d[0];
        free(wm);
       // printf("Residual Error: %lg\n", abs(creal(b[0]))/(pro));

    T[0] = a[0];
    T[1] = b[1];
    for (j=1; j<m-1; j++)
    {
       T[j*m+j-1] = d[j  ];
       T[j*m+j  ] = a[j  ];
       T[j*m+j+1] = b[j+1];
    }
    T[m*m-2] = d[m-1];
    T[m*m-1] = a[m-1];
    free(a);
    free(b);
    free(d);
    free(t);

    return ((creal(b[0]))); //residual error approx
}

