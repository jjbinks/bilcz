#include "buildnew_vect.h"

void buildnew_vect(
        double * Zp,
        double * v,
        double * w,
        int n, int m) {
    int i, id0, id1;
    id0 = rand() % m;
    do id1 = rand() % m;
    while (id0 == id1);
    for (i=0; i<n; i++) {
        v[i] = Zp[i*m + id0] + Zp[i*m + id1];
        w[i] = Zp[i*m + id0] - Zp[i*m + id1];
    }
}

/*
int * sort_value(double complex* val_p, int m){
      int * sorted_pos = (int*) malloc(sizeof(int)*m);
      double *norms = (double*) malloc(sizeof(double)*m);

    int i;
    for (i=0; i<m; i++) {
        sorted_pos[i]=i;
        norms[i]= val_p[i]*conj(val_p[i]);
    }
    //sort
    double swap;
    int swap_p;
    int c,d;
    for (c = 0 ; c < ( m - 1 ); c++)
    {
        for (d = 0 ; d < m - c - 1; d++)
        {
            if (norms[d] < norms[d+1])
            {
               swap       = norms[d];
               norms[d]   = norms[d+1];
               norms[d+1] = swap;

               swap_p          = sorted_pos[d];
               sorted_pos[d]   = sorted_pos[d+1];
               sorted_pos[d+1] = swap_p;

            }
        }
    }
    free(norms);
    return sorted_pos;
}
*/
void
buildnew_vect_complex(
        double complex* Zp,
        double complex* val_p,
        double complex* v,
        double complex* w,
        int n, int m, int k) {


//Find biggest vp
//    int * sorted_pos = sort_value(val_p, m);

    int i;

    for(i=0; i<n; i++ ){
        v[i]=0;
        w[i]=0;
    }

    int j;
    for (i=0; i<n; i++){
        for (j=0; j<k; j++) {
            //v[i] += Zp[i*m +sorted_pos[j]];
            //w[i] += Zp[i*m +sorted_pos[j]];
            v[i] += Zp[i*k + j];
            w[i] += Zp[i*k + j];
            //w[i] += (i%2)? Zp[i*m +j]:-Zp[i*m+j];
            // w[i] += (Zp[i*m+j]==0.)? Zp[i*m +j]: 1/Zp[i*m+j];
            // w[i] += conj(Zp[i*m +j]);
        }
    }
    double norm_v  = 0;
    double norm_w  = 0;
    for (i=0; i<n; i++) {
        norm_v += v[i]*conj(v[i]);
        norm_w += w[i]*conj(w[i]);
    }
    norm_v = sqrt(norm_v);
    norm_w = sqrt(norm_w);

    double max_noise_v = 0.001*(norm_v/(sqrt(2*m)));
    double max_noise_w = 0.001*(norm_w/(sqrt(2*m)));
    double norm_inv_v= 1/(norm_v);
    double norm_inv_w= 1/(norm_w);
    for (i=0; i<n; i++){
        //printf("real noise: %f, im noise: %f\n",creal(noise(max_noise)), cimag(noise(max_noise)));
        v[i] = (((double) rand()/(double)RAND_MAX)*max_noise_v + v[i])*norm_inv_v;
        w[i] = (((double) rand()/(double)RAND_MAX)*max_noise_w + w[i])*norm_inv_w;
    }
//    free(sorted_pos);
//    printf("NORM V, W =%f\n", norm);
}
