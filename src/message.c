#include "message.h"

/* structure of message:
 * error - origin - flag - eigenvalues - eigenvectors
 * in double values */

int message_init(double ** message,
                 BLCZ_Problem * problem)
{
    int size = 2*(problem->nb_eval * (problem->n + 1)) + 3;
    *message = malloc(size*sizeof(double));
    return size;
}

void message_finalize(double * message)
{
    free(message);
}

int message2result(BLCZ_Result * result,
                    double * message,
                    BLCZ_Problem * problem)
{
    int flag;
    int eigen_val_size = problem->nb_eval*sizeof(double complex);
    int eigen_vec_size = eigen_val_size*problem->n;
    result->error = message[0];
    result->origin = (int)message[1];
    flag = (int)message[2];
    memcpy(result->eigenvalues, message + 3, eigen_val_size);
    memcpy(result->eigenvectors,
           ((char *)(message + 3)) + eigen_val_size,
           eigen_vec_size);
    return flag;
}

void result2message(double * message,
                    BLCZ_Result * result,
                    BLCZ_Problem * problem,
                    int flag)
{
    int eigen_val_size = problem->nb_eval*sizeof(double complex);
    int eigen_vec_size = eigen_val_size*problem->n;
    message[0] = result->error;
    message[1] = (double)result->origin;
    message[2] = (double)flag;
    memcpy(message + 3, result->eigenvalues, eigen_val_size);
    memcpy(((char *)(message + 3)) + eigen_val_size,
           result->eigenvectors,
           eigen_vec_size);
}

