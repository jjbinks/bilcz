/* ALL CREDITS TO MatrixMarket for this code that we bearly modified
   from http://math.nist.gov/MatrixMarket/mmio/c/example_read.c */
#include "matrix_io.h"

double get_matrix_from_file(
        char * filename,
        double complex ** A,
        int * size) {
/*
*   Matrix Market I/O example program
*
*   Read a real (non-complex) sparse matrix from a Matrix Market
*   (v. 2.0) file.
*   and copies it to stdout.  This porgram does nothing useful, but
*   illustrates common usage of the Matrix Matrix I/O routines.
*   (See http://math.nist.gov/MatrixMarket for details.)
*
*   Usage:  a.out [filename] > output
*
*   NOTES:
*
*   1) Matrix Market files are always 1-based, i.e. the index of the first
*      element of a matrix is (1,1), not (0,0) as in C.  ADJUST THESE
*      OFFSETS ACCORDINGLY offsets accordingly when reading and writing
*      to files.
*
*   2) ANSI C requires one to use the "l" format modifier when reading
*      double precision floating point numbers in scanf() and
*      its variants.  For example, use "%lf", "%lg", or "%le"
*      when reading doubles, otherwise errors will occur.
*/
    MM_typecode matcode;
    FILE * f;
    int M, N, nnz;
    int i, j, k;
    double val;

    f = fopen(filename, "r");
    if (f == NULL) {
        printf("Error while opening : %s\n",filename);
        exit(1);
    }
    if (mm_read_banner(f, &matcode) != 0) {
        printf("Could not process Matrix Market banner.\n");
        exit(1);
    }

    /* This is how one can screen matrix types if their application */
    /* only supports a subset of the Matrix Market data types.      */

    if (mm_is_complex(matcode)
     || mm_is_dense(matcode)) {
        printf("Sorry, this application does not support ");
        printf("Market Market type: [%s]\n",
               mm_typecode_to_str(matcode));
        exit(1);
    }

    /* find out size of sparse matrix .... */

    if (mm_read_mtx_crd_size(f, &M, &N, &nnz)) {
        exit(1);
    }

    if (N != M) {
        fputs("ERROR : matrix is not square\n", stderr);
        exit(1);
    }

    /* reseve memory for matrices */
    *size = N;
    *A = calloc(N*N, sizeof(double complex));
    if (!*A) {
        fprintf(stderr, "could not allocate %ldMB\n",
                N*N*sizeof(double complex)/1024/1024);
        #ifndef _NO_MPI_
            MPI_Abort(MPI_COMM_WORLD, 1);
        #else
            exit(1);
        #endif
    }

    double frob = 0.0;
    if (mm_is_symmetric(matcode)) {
        for (k=0; k<nnz; k++) {
            fscanf(f, "%d %d %lg\n", &i, &j, &val);
            i--; /* adjust from 1-based to 0-based */
            j--;
            (*A)[i*N+j] = val;
            (*A)[j*N+i] = val;
            frob += 2*val*val;
        }
    } else {
        #ifndef NDEBUG
            printf("Matrix unsym\n");
        #endif
        for (k=0; k<nnz; k++) {
            fscanf(f, "%d %d %lg\n", &i, &j, &val);
            i--; /* adjust from 1-based to 0-based */
            j--;
            #ifndef NDEBUG
                printf("id:%d\n", i*N+j);
            #endif
            (*A)[i*N+j] = val;
            frob += val*val;
        }
    }
    puts("Done parsing");

    if (f != stdin) {
        fclose(f);
    }

    return sqrt(frob);

}

