#include "blcz_method.h"

void my_problem_init(BLCZ_Problem * problem,
                     double complex * A,
                     double A_frob,
                     int dimension,
                     int subspace_dimension,
                     int number_eig,
                     int maxiter,
                     double acceptable_error)
{
    problem->n = dimension;
    problem->m = subspace_dimension;
    problem->nb_eval = number_eig;
    problem->A = A;
    problem->A_frob = A_frob;
    problem->maxiter = maxiter;
    problem->acceptable_error = acceptable_error;
}

void my_random_start(BLCZ_Problem * problem,
                     BLCZ_Input * input)
{
    //srand(666);
    #ifndef NDEBUG
        printf("I WILL CONSIDER NEW RANDOM\n");
    #endif
    int i;
    double sumv, sumw, tmp;
    sumv = 0;
    sumw = 0;
    tmp = 0;
    for (i=0; i<problem->n; i++)
    {
        tmp = input->v[i] = ((double)rand())/RAND_MAX;
        input->w[i] = tmp;
        tmp *= tmp;
        sumv += tmp;
        //tmp = (input->w[i] = ((double)rand())/RAND_MAX);
        //tmp *= tmp;
        sumw += tmp;
    }
    for (i=0; i<problem->n; i++)
    {
        input->v[i] /= sumv;
        input->w[i] /= sumw;
    }
}

void my_input_init(BLCZ_Problem * problem,
                   BLCZ_Input * input)
{
    input->v = malloc(problem->n*sizeof(double complex));
    input->w = malloc(problem->n*sizeof(double complex));
    my_random_start(problem, input);
}

void my_result_init(BLCZ_Problem * problem,
                    BLCZ_Result * result)
{
    result->eigenvalues = malloc(problem->nb_eval*sizeof(double complex));
    result->eigenvectors = malloc(problem->n * problem->nb_eval
                                * sizeof(double complex));
    result->error = +1.0/0.0; /* maximum error */
    result->origin = 0;
}

void my_input_finalize(BLCZ_Input * input)
{
    free(input->v);
    free(input->w);
}

void my_problem_finalize(BLCZ_Problem * problem)
{
    //free(problem->A);
}

void my_result_finalize(BLCZ_Result * result)
{
    free(result->eigenvalues);
    free(result->eigenvectors);
}

void my_result2input(BLCZ_Problem * problem,
                     BLCZ_Result * result,
                     BLCZ_Input * input){
    buildnew_vect_complex(
            result->eigenvectors,
            result->eigenvalues,
            input->v,
            input->w,
            problem->n,
            problem->m,
            problem->nb_eval);
}

void my_inner_data_init(BLCZ_Problem * problem,
                        BLCZ_Inner_Data * data)
{
    int m = problem->m;
    int n = problem->n;
    int nb_eval = problem->nb_eval;
    data->T   = calloc(m*m,  sizeof(double complex));
    data->V   = malloc(m*n * sizeof(double complex));
    data->W   = malloc(m*n * sizeof(double complex));
    data->vec_p = malloc(m*nb_eval * sizeof(double complex));
    data->ERR = malloc(n*m * sizeof(double complex));
    data->diag_val_p = calloc(nb_eval*nb_eval, sizeof(double complex));
}

void my_inner_data_finalize(BLCZ_Inner_Data * data)
{
    free(data->diag_val_p);
    free(data->T    );
    free(data->V    );
    free(data->W    );
    free(data->vec_p);
    free(data->ERR  );
}

