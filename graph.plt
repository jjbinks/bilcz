set   autoscale                        # scale axes automatically
set logscale y 10
#set logscale x 10
unset label                            # remove any previous labels
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set title "Residual error"
set xlabel "cycle number"
set ylabel "Residual error"
plot "log.out" w lp
pause -1
