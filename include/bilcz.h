#ifndef __BILCZ_H__
#define __BILCZ_H__

#include <lapacke.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "bilcz_proj.h"
#include "frobenius.h"
#include "buildnew_vect.h"
#include "eigen_solver.h"
#include "print_vect.h"
#include "blcz_method.h"

#include "matrix_op.h"
#include <complex.h>

BLCZ_Method bilcz;

#endif // __BILCZ_H__
