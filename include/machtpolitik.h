#ifndef __MACHTPOLITIK_H__
#define __MACHTPOLITIK_H__

#include "blcz_method.h"
#include "print_vect.h"
#include "message.h"
/* contains BLCZ_Method bilcz */

#include <string.h>
#ifndef _NO_MPI_
    #include <mpi.h>
#endif
#ifndef NDEBUG
    #include <stdio.h>
#endif
#include <stdlib.h>

typedef struct {
    int                        index;
    BLCZ_Problem_Init        * problem_init;
    BLCZ_Input_Init          * input_init;
    BLCZ_Result_Init         * result_init;
    BLCZ_Method              * method;
    BLCZ_Result2Input        * result2input;
    BLCZ_Input_Finalize      * input_finalize;
    BLCZ_Problem_Finalize    * problem_finalize;
    BLCZ_Result_Finalize     * result_finalize;
    BLCZ_Inner_Data_Init     * inner_data_init;
    BLCZ_Inner_Data_Finalize * inner_data_finalize;
    BLCZ_Random_start        * random_start;
} MachtPolitik_Worker;

typedef
void MachtPolitik_Worker_Init(
        MachtPolitik_Worker * worker,
        int worker_index);

void MachtPolitik_Exec(
        int id,
        MachtPolitik_Worker_Init * worker_init,
        BLCZ_Problem *problem);

#endif /* __MACHTPOLITIK_H__ */

