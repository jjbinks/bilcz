#ifndef __PRINT_VECT_H__
#define __PRINT_VECT_H__

#include <stdio.h>
#include <complex.h>

void print_vect(double * v, int m, int n, char * msg);
void print_vect_complex(double complex * v, int m, int n, char * msg);

#endif // __PRINT_VECT_H__
