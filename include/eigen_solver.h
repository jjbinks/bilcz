#ifndef __EIGEN_SOLVER_H__
#define __EIGEN_SOLVER_H__

#include<lapacke.h>
#include<stdio.h>
void eigen_solver(int n, double *m, double *val_p, double *vec_p);
void eigen_solver_complex(int n, double complex *m, double complex *val_p, double complex *vec_pi, int nb_eval);

#endif // __EIGEN_SOLVER_H__

