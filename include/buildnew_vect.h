#ifndef __BUILDNEW_VECT_H__
#define __BUILDNEW_VECT_H__

#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <math.h>
#include "print_vect.h"


void buildnew_vect_complex(double complex* Zp,double complex *val_p, double complex* v, double complex* w,
                   int n, int m, int k);

#endif // __BUILDNEW_VECT_H__

