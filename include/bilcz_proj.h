#ifndef __BILCZ_PROJ_H__
#define __BILCZ_PROJ_H__

#include <math.h> // sqrt
#include <cblas.h>
#include <stdlib.h> // malloc, free
#include "print_vect.h"
#include <stdio.h>
#include <complex.h>
#include "blcz_method.h"
/*
 * BI-LANCZOS PROJECTION
 * PROJECTION INTO REAL TRIDIAGONAL SYMMETRIC MATRIX
 *
 * in :  n : dimension of original space
 *       A : matrix (n * n)
 *       v : direction of projection vector (n)
 *       m : dimension of projection subspace
 * out : T : projected tridiagonal matrix (m*m)
 *       V : right hand side projection matrix (m * n)
 *       W : conjugate of left hand side projection matrix (m * n)
 *
 * WARNING :
 * T is only overwritten on its tridiagonal. Set it to zeroes beforehand
 * if you want zeroes around the tridiagonal :-) (blame Jean)
 * Jean says : A simple calloc will do the trick. Siouxors !!
 *
 */

void bilcz_proj(
        int n,
        double complex * A,
        double complex * v,
        double complex * w,
        int m,
        double complex * T,
        double complex * V,
        double complex * W);

#endif // __BILCZ_PROJ_H__

