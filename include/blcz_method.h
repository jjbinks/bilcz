#ifndef __BLCZ_METHOD_H__
#define __BLCZ_METHOD_H__

#include <complex.h>
#include <stdlib.h>
#include <stdio.h>
#include "buildnew_vect.h"

/* Let's try to "genericize" bilcz as an BLCZ method,
 * in a view to Unique & Conquer */


/* Input to be given every time method is called,
 * the problem we want to solve with Unite & Conquer.
 * It is up to the user application to allocate A
 * and choose the content by defining a
 * BLCZ_Problem_Init function, and free A
 * by defining a BLCZ_Problem_Finalize function. */

typedef struct {
    int n;              /* Dimension of original space */
    double complex *A; /* complex (n * n) matrix */
    double A_frob;      /* square of A frob norm */
    int m;              /* Dimension of Krylov subspace */
    int maxiter;        /* Maximum number of inner iterations */
    int nb_eval;              /* Number of eigenvalues wanted */
    double acceptable_error;
    /* in the future: adding a value for sensitivity ?
     * would this be useful for unite & conquer ?*/
} BLCZ_Problem;


/* Input to be updated from one method call to another.
 * It is up to the user application to allocate and
 * initialize v & w by defining a BLCZ_Input_Init function,
 * and free v & w by defining a BLCZ_Input_Finalize function. */

typedef struct {
    double complex * v; /* first initial complex vector */
    double complex * w; /* second initial complex vector */
} BLCZ_Input;


/* The resulting data from one method call.
 * It is up to the user application to allocate and free
 * this, so that it can use the results after the U&C execution.
 * (by defining BLCZ_Result_Init & BLCZ_Result_Finalize
 *  functions of course) */
typedef struct {
    double complex * eigenvalues;
    double complex * eigenvectors;
    double error;
    int origin;
} BLCZ_Result;


/************************************
 *                                  *
 *       PROBLEM INITIALIZER        *
 *                                  *
 * (choose `n`, `m` and `maxiter`,  *
 *  allocate `A` and fill it up)    *
 *                                  *
 ************************************/

typedef
void BLCZ_Problem_Init( BLCZ_Problem *,
                        double complex *A,
                        double A_frob,
                        int dimension,
                        int subspace_dimension,
                        int number_eig,
                        int maxiter,
                        double acceptable_error);

BLCZ_Problem_Init my_problem_init;

/***********************************
 *                                 *
 *        INPUT INITIALIZER        *
 *                                 *
 * (choose first `v` and `w` for a *
 *  given problem -- allocate'em)  *
 *                                 *
 ***********************************/
typedef
void BLCZ_Random_start(BLCZ_Problem *,
                       BLCZ_Input *);

BLCZ_Random_start my_random_start;

typedef
void BLCZ_Input_Init(BLCZ_Problem *,
                     BLCZ_Input *);

BLCZ_Input_Init my_input_init;

/**********************************
 *                                *
 *       RESULT INITIALIZER       *
 *                                *
 * (allocate `eigenvalues` and    *
 *  `eigenvectors`)               *
 *                                *
 **********************************/

typedef
void BLCZ_Result_Init(BLCZ_Problem *,
                      BLCZ_Result *);

BLCZ_Result_Init my_result_init;


/*************************
 *                       *
 *   TEMP DATA IN METHOD *
 *                       *
 *************************/

typedef struct{
    double complex * vec_p;
    double complex * T;
    double complex * V;
    double complex * W;
    double complex * ERR;
    double complex * diag_val_p;
} BLCZ_Inner_Data;


/******************************
 *                            *
 *   INIT TEMP DATA IN METHOD *
 *                            *
 ******************************/
typedef
void BLCZ_Inner_Data_Init( BLCZ_Problem *problem,
                           BLCZ_Inner_Data * data);

BLCZ_Inner_Data_Init my_inner_data_init;

/**********************************
 *                                *
 *   FINALIZE TEMP DATA IN METHOD *
 *                                *
 **********************************/
typedef
void BLCZ_Inner_Data_Finalize( BLCZ_Inner_Data *data);

BLCZ_Inner_Data_Finalize my_inner_data_finalize;



/*************************
 *                       *
 *   THE ACTUAL METHOD   *
 *                       *
 *************************/

typedef
void BLCZ_Method(BLCZ_Problem *,
                 BLCZ_Input *,
                 BLCZ_Result *,
                 BLCZ_Inner_Data *);

/* this could in the future return an int for errors */


/* Update BLCZ_Input for iteration k,
 * using BLCZ_Result from iteration k-1
 * and general problem information from BLCZ_Problem */

typedef
void BLCZ_Result2Input(BLCZ_Problem *,
                       BLCZ_Result *,
                       BLCZ_Input *);

BLCZ_Result2Input     my_result2input;

/* free v and w */

typedef
void BLCZ_Input_Finalize(BLCZ_Input *);
BLCZ_Input_Finalize my_input_finalize;
/* free A */

typedef
void BLCZ_Problem_Finalize(BLCZ_Problem *);
BLCZ_Problem_Finalize my_problem_finalize;

/**************************
 *                        *
 *    RESULT FINALIZER    *
 *                        *
 * free `eigenvalues` and *
 * `eigenvectors`         *
 * (and display them ?)   *
 *                        *
 **************************/

typedef
void BLCZ_Result_Finalize(BLCZ_Result *);
BLCZ_Result_Finalize my_result_finalize;

#endif /* __BLCZ_METHOD_H__ */

