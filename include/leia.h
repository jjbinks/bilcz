#ifndef __LEIA_H__
#define __LEIA_H__

#include <stdio.h>
#include <stdlib.h>
#include "blcz_method.h"
#include "message.h"
#ifndef _NO_MPI_
    #include <mpi.h>
#endif

void Leia(BLCZ_Problem * problem,
          BLCZ_Result_Init * result_init,
          BLCZ_Result_Finalize * result_finalize,
          int nworkers);

#endif /* __LEIA_H__ */

