#ifndef __MATRIX_IO_H__
#define __MATRIX_IO_H__

#include "mmio.h"
#include <complex.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#ifndef _NO_MPI_
    #include <mpi.h>
#endif

/* returns square of frobenius norm */
double get_matrix_from_file(
        char * filename,
        double complex ** A,
        int * size);

#endif /*__MATRIX_IO_H__*/
