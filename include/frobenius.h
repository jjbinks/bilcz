#ifndef __FROBENIUS_H__
#define __FROBENIUS_H__

#include <complex.h>
#include <math.h>

double frobenius(double * V, int n, int m);

double frobenius_complex(double complex * V, int n, int m);

#endif // __FROBENIUS_H__

