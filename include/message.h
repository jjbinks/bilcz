#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#include <stdlib.h>
#include "blcz_method.h"
#include <string.h>
#include <stdio.h>
#include <complex.h>

int message_init(double ** message,
                 BLCZ_Problem * problem);

void message_finalize(double * message);

void result2message(double * message,
                    BLCZ_Result * result,
                    BLCZ_Problem * problem,
                    int flag);

/* returns flag */
int message2result(BLCZ_Result * result,
                   double * message,
                   BLCZ_Problem * problem);

#endif /* __MESSAGE_H__ */

