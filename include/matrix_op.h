#ifndef __MATRIX_OP_H__
#define __MATRIX_OP_H__

#include <complex.h>

void matrix_set_to_zero(double complex * mat,
                        int height,
                        int width);
#endif /*__MATRIX_OP_H__*/
