\documentclass[compress]{beamer}

\usepackage[utf8]{inputenc}
%\usetheme{Boadilla}
\usetheme{Berkeley}
%\useoutertheme[footline=authortitle,
%               headline=section]{miniframes}

\usepackage[backend=bibtex]{biblatex}
\bibliography{biblio.bib}

\usepackage{graphicx}
\graphicspath{ {../images/} }

\title{Méthodes de Lanczos et bi-Lanczos}
\author{John Gliksberg \and Jean Perier}
\institute[MIHPS]{Master MIHPS, Université de Versailles Saint-Quentin}
\date{lundi 29 février 2016}

\newlength{\tabspace}
\setlength{\tabspace}{4ex}

\begin{document}

\frame{\titlepage}

\section{Méthode de Lanczos}

\subsection{Description}

\begin{frame}{frametitle}
    \frametitle{Méthode de Lanczos}
    \begin{itemize}
        \item Mise au point par Cornelius Lanczos (1950)
              \footfullcite{lanczos_iteration_1950}
        \item Similaire aux méthodes d'Arnoldi,
              mais dans le cas de matrices hermitienne
        \item La matrice construite dans le sous-espace est tridiagonale
    \end{itemize}
\end{frame}

\subsection{Schéma}

\begin{frame}
    \frametitle{Schéma de l'application de la méthode de Lanczos}
    \begin{center}
        \includegraphics[width=\textwidth,
                         height=0.8\textheight,
                         keepaspectratio]{irlm.png}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Forme de la matrice dans le sous-espace}
    \Large
    \[
        T \in \mathbb{C}^{m\times m} =
        \left(
            \begin{array}{ccccc}
                \alpha_1 & \beta_1^* &           &             &               \\
                \beta_1  & \alpha_2  & \beta_2^* &             &               \\
                         & \beta_2   & \alpha_3  & \ddots      &               \\
                         &           & \ddots    & \ddots      & \beta_{m-1}^* \\
                         &           &           & \beta_{m-1} & \alpha_m
            \end{array}
        \right)
    \]
\end{frame}

\subsection{Algorithme}

\begin{frame}
    \frametitle{Algorithme de Lanczos\footfullcite{ciarlet_solution_2002}}
    \noindent
    \begin{minipage}[t]{.45\linewidth}
        \begin{block}{}
            \(v\) est le vecteur initial\newline
            \(v_1=v/\|v\|, v_0=0, \beta_0=0\) \newline
            \textbf{for} \(j=1,2...,m-1\) \newline
            \indent\hspace{\tabspace} \(t=Av_j-\beta_{j-1}v_{j-1}\)\newline
            \indent\hspace{\tabspace} \(\alpha_j=v^*_jt\)\newline
            \indent\hspace{\tabspace} \(t=t-\alpha_jv_j\)\newline
            \indent\hspace{\tabspace} \(\beta_j=\|t\|_2\)\newline
            \indent\hspace{\tabspace} \(v_{j+1}=t/\beta_j\)
        \end{block}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{.45\linewidth}
        \begin{block}{}
            On obtient ainsi la matrice de passage \(V_m\)
            et la matrice tridiagonale \(T_{m,m}\)
            telles que \(V^*_mAV_m=T_{m,m}\)
        \end{block}
    \end{minipage}
\end{frame}

\section{Méthode de bi-Lanczos}

\subsection{Description}

\begin{frame}
    \frametitle{Méthode de bi-Lanczos}
    \begin{itemize}
        \item La méthode précédente ne s'applique que pour
              les matrices hermitienne
        \item La méthode bi-Lanczos généralise cette méthode
              à toute matrice\footfullcite{lanczos_iteration_1950}
    \end{itemize}
\end{frame}

\subsection{Schéma}

\begin{frame}
    \frametitle{Schéma de l'application de la méthode de bi-Lanczos}
    \begin{center}
        \includegraphics[width=\textwidth,
                         height=0.8\textheight,
                         keepaspectratio]{irblm}
    \end{center}
\end{frame}

\subsection{Algorithme}

\begin{frame}
    \frametitle{Algorithme de bi-Lanczos\footfullcite{ciarlet_solution_2002}}
    \noindent
    \begin{minipage}[t]{.45\linewidth}
        \begin{block}{}
            \footnotesize
            \(v_1\) et \(w_2\) deux vecteurs normaux initiaux tels que \(w^*_1v_1=\delta_1\neq0 \)\newline
            \(v_0=0, w_0=0, \beta_0=0\) \newline
            \textbf{for} \(j=1,2...,m-1\) \newline
            \indent\hspace{\tabspace} \(t=Av_j-\beta_{j-1}v_{j-1}\)\newline
            \indent\hspace{\tabspace} \(\alpha_j=w^*_jt/\delta_j\)\newline
            \indent\hspace{\tabspace} \(t=t-\alpha_jv_j\)\newline
            \indent\hspace{\tabspace} \(\gamma_{i+1}=\|t\|_2\)\newline
            \indent\hspace{\tabspace} \(v_{j+1}=t/\gamma_{j+1}\)\newline
            \indent\hspace{\tabspace} \(t=\frac{A^*w_j-\beta_{j-1}w_{j-1}-\alpha_jw_j}{\gamma_{j+1}}\)\newline
            \indent\hspace{\tabspace} \(\delta_{j+1}=w^*_{j+1}v_{j+1}\)\newline
            \indent\hspace{\tabspace} \(\beta_j=\gamma_{j+1}\delta_{j+1}/\delta_j\)\newline
        \end{block}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{.45\linewidth}
    \begin{block}{}
        On obtient ainsi des matrices \(V_m\) et \(W_m\) ainsi que
        la matrice diagonale \(D_m\) et la matrice tridiagonale \(T_{m,m}\)
        telles que \(W^*_mAV_m=D_mT_{m,m}\)
    \end{block}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Forme de la matrice dans le sous-espace}
    \Large
    \[
        T \in \mathbb{C}^{m\times m} =
        \left(
            \begin{array}{ccccc}
                \alpha_1 & \beta_1  &          &              &             \\
                \gamma_1 & \alpha_2 & \beta_2  &              &             \\
                         & \gamma_2 & \alpha_3 & \ddots       &             \\
                         &          & \ddots   & \ddots       & \beta_{m-1} \\
                         &          &          & \gamma_{m-1} & \alpha_m
            \end{array}
        \right)
    \]
\end{frame}

\begin{frame}
    \begin{block}{}
        $T$ est tridiagonale non-symmétrique.

        On détermine les eigenvalues/vectors avec {\tt zgeev} (généraliste).

        Nous n'avons pas trouvé de routine QR spécifique aux matrices
        tridiagonales/Heisenberg qui renvoyait les vecteurs propres.

        On récupère ainsi {\tt val\_p} et {\tt vec\_p}
        (qui est reprojetée en {\tt Zp} dans l'espace de départ).
    \end{block}
    \begin{block}{Calcul de l'erreur}
        Pour mesurer la qualité des {\tt val\_p} et {\tt Zp} sortants,
        on calcule
        \[
            \frac{\left\| A Z_p - val_p Z_p \right\|}
                 {\left\|A\right\|}
        \]
        Avec une norme équivalente à la norme de Frobenius.
    \end{block}
\end{frame}

\subsection{Séquentiel}

\begin{frame}
    \frametitle{Matrices de test}
    \begin{itemize}
        \item Nous avons utilisé des matrices symmétriques et non-symmétriques,
              prévues pour des problèmes de valeurs propres. \\
              Les courbes présentées correspondent à des résolutions sur
              {\tt ck656}, non symmétrique, de taille $656^2$
        \item Les resultats en valeurs propres sont comparés à ceux de Scilab.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Redémarrage}
    On choisit les valeurs propres au plus grands modules,
    et on choisit comme vecteur de direction leur combinaison linéaire
    (normalisée, bruitée)

    On préférerait prendre en compte la précision des valeurs propres
    dans leur choix / pondération.
    Mais ce n'est pas le cas dans notre code actuel.
\end{frame}

\begin{frame}
    \frametitle{Contrôle de recherche (Shake \& Explore)}
    Nous estimons que notre projection + QR sont (très) bons,
    mais que notre redémarrage éloignait fortement notre
    champ de recherche de l'espace de solution déterminé
    dans le sous-espace de Krylov.

    \begin{minipage}{.5\linewidth}
        \includegraphics[width=\textwidth,
                         height=0.65\textheight,
                         keepaspectratio]{ck656_w1_md1000}
    \end{minipage}
    \hfill
    \begin{minipage}{.45\linewidth}
        Matrice de taille $656^2$, \\
        taille de sous-espace 25, \\
        5 valeurs propres souhaitées
    \end{minipage}
\end{frame}

\begin{frame}
    Tentative de correction par pseudo arbre de recherche
    (phases de recherche interrompues par des jets aléatoires).

    \includegraphics[width=\textwidth,
                     height=0.8\textheight,
                     keepaspectratio]{irblm_restart}
\end{frame}

\begin{frame}
    \begin{minipage}{.5\linewidth}
        \includegraphics[width=\textwidth,
                         height=0.8\textheight,
                         keepaspectratio]{ck656_w1_md5_1000it}
    \end{minipage}
    \hfill
    \begin{minipage}{.45\linewidth}
        Même problème \\
        profondeur de recherche = 5 cycles
    \end{minipage}
\end{frame}

\begin{frame}
    \centering
    \includegraphics[width=\textwidth,
                     height=0.8\textheight,
                     keepaspectratio]{ck656_w1_md5}
\end{frame}

\subsection{Parallèle}

\begin{frame}
    \frametitle{Multithreading}

    On a parallélisé les routines BLAS de niveau 2
    présentes lors de la projection.

    Nous n'avons pas observé de speedup, même sur {\tt af25860.mtx}
    (de taille $23K^2$).

    Nous souhaiterions également paralléliser les
    routines BLAS de niveau 3 présentes lors de la
    reprojection et du calcul d'erreur.

    De toute manière nous attendons beaucoup plus de speedup
    d'un passage en creux (au moins dans l'espace de départ)
    que de multithreading.
\end{frame}

\subsection{Instances multiples}

\begin{frame}
    \frametitle{Instances multiples (Unite \& Conquer)}
    Le étapes de calcul correspondant à bi-Lanczos ont
    été encapsulées et nous avons décrit nos résolutions
    par des contrats en terme de
    \begin{itemize}
        \item Problème global
              ({\it A, nombre de valeurs souhaitées})
        \item Input local
              ({\it vecteurs de direction, taille du sous-espace})
        \item Resultat local / global ({\it synchro, maj input})
        \item Inner data ({\it pour éviter les allocations})
    \end{itemize}

    Ainsi on a pu faire travailler en parallèle des méthodes
    en pouvant faire jouer des paramètres de résolution
    pour éviter des effets de bords.
\end{frame}

\begin{frame}
    \frametitle{Architecture du Unite \& Conquer}
    \includegraphics[width=\textwidth,
                     height=0.8\textheight,
                     keepaspectratio]{arch_gen}
\end{frame}

\begin{frame}
    \frametitle{Architecture worker}
    \includegraphics[width=\textwidth,
                     height=0.8\textheight,
                     keepaspectratio]{irblm_mpi}
\end{frame}

\begin{frame}
    \frametitle{Architecture décideur}
    \includegraphics[width=\textwidth,
                     height=0.8\textheight,
                     keepaspectratio]{decision}
\end{frame}

\end{document}

