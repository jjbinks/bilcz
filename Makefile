CC := mpicc
EXEC := mpiexec
MAXIT := 100   # MAX NUMBER OF ITERATIONS PER WORKER
MAX_DEPTH := 5 # MAX NUMBER OF ITERATIONS WITHOUT IMPROVEMENTS
               # BEFORE RANDOM RESTART
KS := 25       # SIZE OF KRILOV SUBSPACE (SINCE WE DON'T KNOW
               # HOW TO DETERMINE IT FROM THE MATRIX ONLY)
NV := 5        # NUMBER OF DESIRED EIGENVALUES (COULD ALSO
               # HAVE A REASONABLE DEFAULT DETERMINED FROM MATRIX)
EPS := 1e-6    # ACCEPTABLE ERROR
CFLAGS := -std=c99 -Wpedantic -Wall -Ofast -ffast-math \
          -march=native -fopenmp -Iinclude -I/usr/lib/openmpi/include \
          -DMAXIT=$(MAXIT) -DMAX_DEPTH=$(MAX_DEPTH) \
          -DKS=$(KS) -DNV=$(NV) -DACCEPTABLE_ERROR=$(EPS)
LDFLAGS := -lblas -llapacke -lm
TARGET := bilcz

NP := 2                  # NUMBER OF PROCESSES = NUMBER OF WORKERS + 1
A := matrix/bcsstk08.mtx # INPUT MATRIX

.PHONY: all release nompi run clean

release: CFLAGS += -DNDEBUG
release: $(TARGET)

nompi: CFLAGS += -D_NO_MPI_
nompi: CC := gcc
nompi: all

all: CFLAGS += -g
all: $(TARGET)

$(TARGET): src/main.c obj/bilcz.o obj/bilcz_proj.o obj/frobenius.o \
           obj/buildnew_vect.o obj/print_vect.o obj/eigen_solver.o \
           obj/blcz_method.o obj/message.o obj/leia.o \
           obj/machtpolitik.o obj/mmio.o obj/matrix_io.o \
           include/bilcz.h include/bilcz_proj.h include/frobenius.h \
           include/buildnew_vect.h include/print_vect.h \
           include/eigen_solver.h include/blcz_method.h \
           include/message.h include/leia.h include/machtpolitik.h \
           include/mmio.h include/matrix_io.h
	$(CC) obj/bilcz.o obj/bilcz_proj.o obj/frobenius.o \
	      obj/buildnew_vect.o obj/print_vect.o obj/eigen_solver.o \
	      obj/blcz_method.o obj/message.o obj/leia.o \
	      obj/machtpolitik.o obj/mmio.o obj/matrix_io.o \
	      src/main.c             -o bilcz \
	      $(CFLAGS) $(LDFLAGS)

obj/bilcz.o: src/bilcz.c include/bilcz.h include/bilcz_proj.h \
             include/frobenius.h include/buildnew_vect.h \
             include/print_vect.h include/eigen_solver.h \
             include/blcz_method.h
	$(CC) src/bilcz.c         -c -o obj/bilcz.o         $(CFLAGS)

obj/bilcz_proj.o: src/bilcz_proj.c include/bilcz_proj.h
	$(CC) src/bilcz_proj.c    -c -o obj/bilcz_proj.o    $(CFLAGS)

obj/frobenius.o: src/frobenius.c include/frobenius.h
	$(CC) src/frobenius.c     -c -o obj/frobenius.o     $(CFLAGS)

obj/buildnew_vect.o: src/buildnew_vect.c include/buildnew_vect.h
	$(CC) src/buildnew_vect.c -c -o obj/buildnew_vect.o $(CFLAGS)

obj/print_vect.o: src/print_vect.c include/print_vect.h
	$(CC) src/print_vect.c    -c -o obj/print_vect.o    $(CFLAGS)

obj/eigen_solver.o: src/eigen_solver.c include/eigen_solver.h
	$(CC) src/eigen_solver.c  -c -o obj/eigen_solver.o  $(CFLAGS)

obj/machtpolitik.o: src/machtpolitik.c include/machtpolitik.h \
                    include/blcz_method.h
	$(CC) src/machtpolitik.c  -c -o obj/machtpolitik.o  $(CFLAGS)

obj/blcz_method.o: src/blcz_method.c include/blcz_method.h
	$(CC) src/blcz_method.c   -c -o obj/blcz_method.o   $(CFLAGS)

obj/leia.o: src/leia.c include/leia.h include/blcz_method.h
	$(CC) src/leia.c          -c -o obj/leia.o          $(CFLAGS)

obj/message.o: src/message.c include/message.h include/blcz_method.h
	$(CC) src/message.c       -c -o obj/message.o       $(CFLAGS)

obj/mmio.o: src/mmio.c include/mmio.h
	$(CC) src/mmio.c          -c -o obj/mmio.o          $(CFLAGS)

obj/matrix_io.o: src/matrix_io.c include/matrix_io.h include/mmio.h
	$(CC) src/matrix_io.c     -c -o obj/matrix_io.o     $(CFLAGS)

run: $(TARGET)
	$(EXEC) -np $(NP) ./$(TARGET) $(A)

clean:
	rm -f $(TARGET) *.o

