Bi-Lanczos with Shake & Explore Unite & Conquer
===============================================

Jean Perier & John Gliksberg

##1. Dependencies
- MPI
- OpenMP
- Blas
- Lapacke

##2. Compiling the project

###2.1 Bi-Lanczos parameters that can be defined at compile time

Parameter   | Definition                                                                             | Default value
----------- | -------------------------------------------------------------------------------------- | -------------
`NV`        | Number of eigenvalues to be found                                                      | 5
`KS`        | Size of Krylov subspace                                                                | 25
`MAX_DEPTH` | Number of consecutive iteration without improvement before restart with random vectors | 5
`MAXIT`     | Maximum number of iterations allowed for each worker                                   | 100
`EPS`       | Acceptable error, the program will stop if the error is below this value               | 1e-6

- To force recompile the project to change parameters, use the `-B` flag.

###2.2 Release compiliation

- `make release`
- `make release NV=10 KS=100`
- `make -B release NV=10 KS=100`

###2.3 Debug compilation

Enable GDB debugging

- `make debug`

###2.4 Compile without MPI

To compile without MPI

- `make nompi`

##3. Running the project

###3.1 Bi-Lanczos parameters can be passed as arguments

Parameter | Definition                       | Default value
--------- | -------------------------------- | -------------
`A`       | Path to a mtx matrix market file | `matrix/bcsstk08.mtx`
`NP`      | Number of MPI processes          | 2

###3.2 Running the program

- `make run`
- `make run NP=6`

##4. Usage

Do stuff like

- `make -B && make run`
- `make -B release && make run`
- `make -B release run KS=12 NV=8 NP=5 MAXIT=40 MAX_DEPTH=3 EPS=1e-5`
- `make clean`

