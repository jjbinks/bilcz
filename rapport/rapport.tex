\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{newcent}
\selectfont
\usepackage[francais]{babel}
%\usepackage[margin=1.1in]{geometry}
\usepackage{graphicx}
\graphicspath{ {../images/} }
\usepackage{subcaption}
\usepackage{algorithm2e}
\usepackage{microtype}
\usepackage{hyperref}
\usepackage{url}

\title{Méthode de Bi-Lanczos}
\author{John Gliksberg \and Jean Perier}

\begin{document}
\maketitle

\vspace{\fill}

\section*{Résumé}
{
    \large
    Les problèmes de valeurs propres
    se retrouvent dans de nombreux domaines
    notamment ceux qui cherchent à valoriser
    les données massives.
    La tailles des matrices en jeu
    mènent à l'utilisation de méthode
    d'approximation numériques,
    souvent itératives.
    La méthode de Lanczos propose
    une réponse astucieuse à ce problème.
    Cependant, elle n'est valide
    que dans le cas des matrices
    à symétrie hermitienne.
    C'est pourquoi la méthode de Bi-Lanczos
    a été mise au point,
    pour proposer une approche similaire
    à celle de Lanczos
    dans le cas des matrices quelconques.
    Cet article propose des implémentations
    séquentielle et parallèle de cette méthode
    et propose aussi un couplage avec l'approche
    ``Unifier pour Conquérir''.
    La recherche et le contrôle du redémarrage
    sont dirigés par une approche ``Secouer et Explorer''
    pour éviter des cas pathologiques.
}

\vspace{\fill}

\newpage

\section{Introduction}

    La méthode de bi-Lanczos est une méthode
    itérative dans les espaces de Krylov.
    Elle a été introduite par Lanczos dans
    \cite{lanczos_iteration}.
    C'est une méthode dérivée de Lanczos
    également décrite dans
    \cite{lanczos_iteration}.

    Cette dernière méthode qui s'applique uniquement
    sur les matrices à symétrie hermitienne
    permet de d'obtenir une matrice projetée
    dans l'espace de Krylov sous la forme d'une matrice
    tridiagonale symétrique.
    Cette forme permet notamment
    d'accélérer les étapes suivantes.
    En effet, la méthode QR est utilisée
    dans le sous-espace de Krylov
    pour résoudre le problème des paires de Ritz;
    la forme tridiagonale symétrique
    permet à cette méthode de converger
    plus rapidement.

    La méthode de bi-Lanczos permet, elle,
    d'aboutir à une matrice tridiagonale
    pas forcément symétrique dans l'espace de Krylov,
    à partir d'une matrice quelconque.
    Pour obtenir ce résultat,
    ce n'est plus une unique matrice orthogonale
    de passage qu'il est nécessaire de construire
    mais deux matrices orthogonales
    pour multiplier à respectivement
    droite et à gauche la matrice de départ.

    Cette différence double les opérations matrices vecteurs
    lors de la construction des matrices.
    De plus, la résolution approximative du problème de
    valeurs / vecteurs propres dans le sous-espace est
    un peu plus difficile et demande d'utiliser des méthodes
    généralistes car la matrice n'y est plus nécessairement symétrique.
    On notera cependant que cette matrice reste tridiagonale et que
    c'est ce qui différencie la méthode de bi-Lanczos par rapport
    à d'autres méthodes itératives pour résoudre les problèmes
    de valeurs propres comme celles d'Arnoldi.
    En effet, ces méthodes s'appliquent également sur des matrices
    quelconques, mais elles sont sans garanties sur la forme
    de la matrice dans le sous-espaces.

\section{Description de la méthode et de l'algorithme}

    \subsection{Méthode Séquentielle}
    \label{sec:methode_seq}

        Chaque itération de la méthode de bi-Lanczos se décompose
        en quatre étapes.

        \subsubsection{Projection de bi-Lanczos}

            La première étape consiste à effectuer
            la projection dans l'espace de Krylov.
            Pour cela nous utilisons
            l'algorithme présenté dans
            \cite{ciarlet_solution}.
            L'algorithme~\ref{alg:proj} décrit
            l’algorithme utilisé dans cette étape.

            \begin{algorithm}
                $v_1$ et $w_1$ deux vecteurs normaux
                initiaux tels que $w_1^* v_1 = \delta_1 \neq 0$\;
                $v_0 = 0, w_0 = 0, \beta_0 = 0$\;
                \For{$j = 1, 2, ..., m-1$}{
                    $\alpha_j = (A x_j,w_j)$\; %w^*_jt/\delta_j$\;
                    $v_{j+1} = A v_j - b_j v_{j-1} - a_j v_j$\;
                    $w_{j+1} = A^H w_j
                             - \delta_{j-1} w_{j-1}
                             - \alpha_j w_j$\;
                    $\delta_{j+1} =
                        \left|\left(
                            v_{j+1}, w_{j+1}
                        \right)\right|^{\frac{1}{2}}$\;
                    $\beta_{j+1} =
                        \left(
                            v_{j+1}, w_{j+1}
                        \right)/\delta_j$\;
                    $w_{j+1} = w_{j+1}/\beta_{j+1}$\;
                    $v_{j+1} = v_{j+1}/\delta_{j+1}$\;
                }
                \caption{Algorithme de projection pour bi-Lanczos}
                \label{alg:proj}
            \end{algorithm}

            \`A la fin de la projection
            on obtient donc une matrice tridiagonale $T$
            avec les coefficient $\alpha_j$ sur la diagonale,
            $\beta_j$ sur la diagonale supérieure
            et $\gamma_j$ sur la diagonale inférieure.

            On obtient ainsi des matrices \(V\) et \(W\)
            ainsi que la matrice tridiagonale \(T\)
            telles que \(W^* A V = T\).
            Comme le souligne l'article
            \cite{van_der_veen_bi-lanczos},
            si l'on choisit $v_1$ et $w_1$
            tels que $|v_1^* w_1| = 1$
            alors on a $V^* W = W^* V = I_m$.

            Soit $x$ un vecteur de taille $m$,
            on peut facilement montrer que
            si $V x$ est un vecteur propre de $A$
            de valeur propre $\lambda$
            alors $x$ est un vecteur propre de $T$.

            En effet on a:

            \[
                Tx = W^* A V x
                   = W^* (\lambda V x)
                   = W^* V (\lambda x)
                   = I_m (\lambda x) = \lambda x
            \]

            Cette remarque mène à considérer
            les valeurs propres de $T$ et leur image par $V$
            comme des approximations des valeurs propres de $A$.
            Une preuve plus complète peut être trouvée dans
            \cite{ciarlet_solution}

            On notera que notre implémentation
            pourrait encore être améliorée
            en prenant en compte le fait que $W^* V$
            n'est pas tout à fait égal à $I_m$
            à cause de l'imprécision numérique de la machine.
            Il serait possible de remédier à cela en appliquant une
            ré-orthogonalisation partielle durant la constructions des
            $v_k$ et $w_k$ avec la méthode introduite dans
            \cite{van_der_veen_bi-lanczos} par example.

        \subsubsection{Recherche des paires de Ritz}

            La seconde étape consiste donc
            à résoudre le problème des paires de Ritz de $T$,
            ce que l'on fait à partir de la méthode QR.

            C'est ici notamment que la méthode de bi-Lanczos
            se distingue de la méthode d'Arnoldi,
            car $T$ est tridiagonal,
            ce qui facilite la recherche
            des valeurs propres en général.
            Cependant le caractère non symétrique
            de la matrice ne nous permet pas
            d'utiliser des méthodes spécialisées
            dans cette résolution.

            La résolution nous donne $m$ valeurs propres
            et leurs vecteurs propres associés.
            On ne garde que les $nv$ valeurs propres
            les plus grandes,
            où $nv$ est le nombre de valeurs propres recherchées.

        \subsubsection{Approximations des vecteurs propres de A}

            La dernière étape consiste ensuite
            à multiplier les vecteurs propres obtenus
            à gauche par V pour obtenir
            les nouvelles approximations
            des vecteurs propres de $A$.

            Notre implémentation séquentielle de cet algorithme
            est écrit en langage C
            et se base sur l'utilisation des librairies
            BLAS pour les opérations matricielles
            et LAPACK pour la méthode QR.

    \subsection{Redémarrage explicite de la méthode}

        Dans l'approche itérative on reconstruit ensuite des
        nouveaux vecteurs initiaux $v_1$ et $w_1$ à partir de
        ces approximations pour construire un nouvel espace de Krylov.

        Dans une première approche, on se contente de construire
        $v_1$ et $w_1$ égaux comme la combinaison linéaire avec des
        poids égaux des approximations de vecteurs.
        On normalise ce dernier vecteur,
        ce qui permet de nous assurer de la condition $|w_1^* v_1| = 1$.

        On itère tant que le rapport de la norme du résidu sur la norme
        de Frobenius de $A$ est supérieur à une constante que l'on fixe.

        Soit $Z$ la matrice des vecteurs propres et
        $D$ la matrice contenant les valeurs propres associées
        sur la diagonale et zéro ailleurs.
        On a alors la condition suivante:

        \[
            \mbox{r\'esidu} = \frac{||A Z - D Z||}{||A Z||}
                            \leq \epsilon
        \]

        Cette mesure de l'erreur est utilisée plus tard
        dans les mesures de convergence (partie~\ref{sec:exp_seq}).

        On note que pour un vecteur propre $Vx$
        et sa valeur propre associée $\lambda$,
        on peut normalement approximer l'erreur $||A Z - D Z||$
        par $|\beta_m x|$.

        En effet d'après \cite{van_der_veen_bi-lanczos}
        on a $AV = V_{m+1}T_{m+1}$ avec:

        \[
            T_{m+1} =
            \left[
                \begin{array}{c}
                    T\\
                    0, \cdots, 0,\ \beta_m
                \end{array}
            \right] \quad et \quad V_{m+1} =
            \left[
                V,\ v_{m}
            \right]
        \]

        On en déduit:

        \[
            A V x - \lambda V x = A V x - V T x
            = (A V - V T) x = (\beta_m v_m) x
        \]

        Nous avons implémenté cette mesure d'erreur mais elle
        ne concorde pas toujours avec le calcul de $||AZ-DZ||$.
        On explique cet écart par le fait que notre espace de Krylov
        peut être imparfait car nous ne procédons pas à
        une ré-orthogonalisation de $V$ et $W$.
        Nous avons donc conservé le calcul d'erreur complet
        pour contrôler l'algorithme pour l'instant,
        bien que cela soit très couteux en performance.

    \subsection{Validation de la méthode}

        Nous avons validé l'implémentation de
        l'algorithme séquentiel en le testant
        d'abord sur de petites matrices
        et en validant les résultats à la main
        ou avec le logiciel Scilab.

        Nous avons ensuite mené des tests
        sur des matrices de plus grandes tailles
        issues de Matrix Market \cite{matrix_market}.
        Nous avons testé les matrices
        {\tt bcsstk08.mtx} (symétrique)
        et {\tt ck656.mtx} (non symétrique)
        et avons vérifié, toujours avec Scilab,
        que les valeurs propres que nous obtenions étaient correctes.

    \subsection{Contrôle de recherche (Shake \& Explore)}
    \label{sec:methode_sae}

        \subsubsection{Constat du blocage de la méthode itérative}

            La méthode précédente ne permet pas
            de converger vers les valeurs propres
            de manière robuste.
            En effet, on observe que la méthode
            converge souvent sur les quelques premières itérations,
            puis la solution se dégrade
            et la méthode ne converge plus
            ou très rarement vers une solution optimale.

            On suppose que ce phénomène est dû en partie
            au redémarrage de la méthode qui est assez basique
            (combinaison linéaire des approximations
             de vecteurs propres déterminées)
            et qui pousse sans doute la méthode
            à tourner en rond dans
            un "mauvais" sous-espace vectoriel.

        \subsubsection{Proposition d'une méthode adaptative pour
                       palier au blocage}

            On décide donc de palier à ce blocage
            en donnant une capacité d'adaptation
            à la méthode itérative.
            Pour cela, il faut dans un premier temps
            détecter que la méthode est bloquée
            dans un mauvais sous-espace.
            Dans un second temps, la méthode doit alors
            opter pour un redémarrage qui lui permettra
            de sortir de ce sous-espace.

            On peut donc séparer la méthode en deux phase distinctes.
            La première phase est une phase d'exploration
            où l'on applique la méthode itérative
            décrite précédemment avec des redémarrage classiques.
            La deuxième phase,
            qui vient entrecouper la première lorsqu'elle est bloquée,
            est une phase où l'on redéfinit un nouveau
            point d'entrée pour l'exploration.

        \subsubsection{Première implémentation de la méthode}

            Dans un premier temps,
            nous proposons des solutions simples
            pour la détection du blocage
            et le comportement qui s'ensuit.
            On détecte tout simplement un blocage en comptant
            le nombre de redémarrage normaux
            qui sont effectués successivement
            sans améliorer la meilleur solution.

            On dispose d'un compteur que l'on remet à zéro
            à chaque fois que l'on trouve une meilleur solution
            et que l'on incrémente sinon.
            Lorsque ce compteur dépasse une certaine constante fixée,
            on décide de définir un nouveau point d'entrée et
            de remettre ce compteur à zéro.

            La solution de redéfinition d'un point d'entrée
            que nous proposons dans un premier temps
            consiste à choisir des nouveaux vecteurs
            de départ aléatoirement.

            Une première description est proposée
            figure~\ref{fig:irblm_restart}.
            L'implémentation est discutée partie~\ref{sec:unc}.

            Bien que basiques, ces deux solutions
            amènent déjà une nette amélioration
            lors de la recherche de solution,
            il n'y a plus de blocage,
            et on a même une quasi-garantie
            qu'avec un nombre de tirage de points d'entrée
            suffisamment grand,
            on finira par trouver une solution qui nous satisfait.

        \subsubsection{Proposition d'amélioration
                       de l'implémentation de la méthode}

            Cependant cette approche basée sur un aléatoire
            de "force brute" ne peut pas laisser espérer
            passer à l'échelle sur des matrices gigantesques.
            Il serait nécessaire de revoir les deux phases
            avec des méthodes plus intelligentes
            qui sont capables de considérer les solutions
            et les sous-espaces qui ont étés rencontrés
            pour prendre des décisions.

            On pourrait ainsi penser à une solution
            de détection des blocages qui n'est pas
            basée sur une simple constante
            mais qui adapte son seuil de tolérance
            en fonction de l'évolution des résultats
            et du nombre de constructions de nouveaux points d'entrée.
            Pour ce qui est de la construction
            de nouveaux points d'entrée,
            une des premières solutions plus poussées
            serait de chercher à sortir de l’espace
            dans lequel la méthode est coincée
            en construisant un vecteur de départ
            orthogonal à ce sous-espace.

            D'une manière générale,
            il nous semblerait intéressant
            de réussir à coupler la méthode itérative de bi-Lanczos
            avec des méthodes d'optimisation
            basées sur l'exploration de graphe par example
            pour choisir des vecteurs d'entrée
            qui balayent l'espace et tiennent comptent
            des résultats qui ont été trouvés.

            \begin{figure}
                \centering
                \includegraphics[width=.8\textwidth]{irblm_restart}
                \caption{Description Shake \& Explore}
                \label{fig:irblm_restart}
            \end{figure}

    \subsection{Méthode Parallélisme}

        On a parallélisé les routines BLAS de niveau 2
        présentes lors de la projection, à l'aide d'OpenMP.
        Nous n'avons pas observé de speedup, même sur {\tt af23560.mtx}
        (de taille $23K^2$).

        Nous souhaiterions également paralléliser les
        routines BLAS de niveau 3 présentes lors de la
        reprojection et du calcul d'erreur.
        De toute manière nous attendons beaucoup plus de speedup
        d'un passage en creux (au moins dans l'espace de départ)
        que de multithreading.

    \subsection{Instances multiples ({\it Unite \& Conquer})}
    \label{sec:unc}

        Le étapes de calcul correspondant à bi-Lanczos ont
        été encapsulées et nous avons décrit nos résolutions
        par des contrats en terme de
        \begin{itemize}
            \item Problème global
                  ({\it A, nombre de valeurs souhaitées})
            \item Entrée locale
                  ({\it vecteurs de direction, taille du sous-espace})
            \item Resultat local / global ({\it synchro, maj input})
            %\item Inner data ({\it pour éviter les allocations})
        \end{itemize}

        Ainsi on a pu faire travailler en parallèle des méthodes
        en pouvant faire jouer des paramètres de résolution
        pour éviter des effets de bords.

        Cette méthode a été implémentée en parallèle du contrôle
        de recherche. Ainsi l'organisation de la recherche est
        découpée entre plusieurs instances.
        Ce n'est pas nécessaire pour implémenter le contrôle de
        recherche mais le découpage était naturel.

        Une instance ``décideuse'' contrôle l'exécution de plusieurs
        instances ``travailleuses''.
        C'est l'instance décideuse qui donne aux travailleurs les
        vecteurs en entrée sur lesquels travailler, dans un modèle
        serveur/client, et c'est à ce niveau que le choix d'un
        redémarrage aléatoire se fait. Voir les
        figures~\ref{fig:arch_unc},~\ref{fig:arch_worker} et
        ~\ref{fig:arch_leia}.

        Les communications sont décrites par des appels MPI.

        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{arch_gen}
            \caption{Architecture Unite \& Conquer}
            \label{fig:arch_unc}
        \end{figure}

        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{irblm_mpi}
            \caption{Architecture travailleur}
            \label{fig:arch_worker}
        \end{figure}

        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{decision}
            \caption{Architecture décideur}
            \label{fig:arch_leia}
        \end{figure}

\section{Expérimentation}

    \subsection{Séquentielle}
    \label{sec:exp_seq}

        La mesure de la convergence correspond au calcul d'erreur
        décrit partie~\ref{sec:methode_seq}.

        Le comportement de la résolution est assez chaotique:
        selon les dimensions et complexité du problème,
        une solution est parfois trouvée en plusieurs
        cycles de redémarrage, mais lorsque ce n'est pas le cas,
        le nombre de cycles nécéssaires peut monter à plusieurs
        miliers ou même jamais.

        Les graphes de convergence nous aident
        à visualiser le comportement.
        Voir la figure~\ref{fig:conv_seq},
        qui décrit la convergence de la méthode
        sur un problème de taille $656^2$,
        composé principalement de trois bandes.
        Tous les graphes de convergence présentés
        correspondent à des problèmes qui recherchent
        5 valeurs propres avec une taille
        de sous-espace de Krylov de 25.

        \begin{figure}
            \centering
            \includegraphics[width=.7\textwidth]{ck656_w1_md1000}
            \caption{Convergence en séquentiel
                     sans redémarrage aléatoire}
            \label{fig:conv_seq}
        \end{figure}

        Après une première solution avec une erreur de l'ordre
        de $10^{-4}$, on ne s'approche plus jamais de cet ordre
        de grandeur.

    \subsection{Shake \& Explore}

        C'est pour remédier à ce comportement qu'on a implémenté
        le redémarrage aléatoire (partie~\ref{sec:methode_sae}).

        Les figures~\ref{fig:conv_sae_1000} et~\ref{fig:conv_sae}
        montrent sur le même problème,
        que pour la figure~\ref{fig:conv_seq},
        le comportement de la méthode avec 1000 itérations
        puis un ``zoom'' sur les premières itérations
        pour visualiser la forme en zig-zag.

        La figure ~\ref{fig:conv_sae} a été réalisée
        avec un redémarrage aléatoire toutes les cinq itérations
        sans amélioration.

        \begin{figure}
            \centering
            \subcaptionbox{
                1000 itérations \label{fig:conv_sae_1000}
            }{
                \includegraphics[width=.47\textwidth]{ck656_w1_md5_1000it}
            }
            \subcaptionbox{
                100 itérations \label{fig:conv_sae}
            }{
                \includegraphics[width=.47\textwidth]{ck656_w1_md5}
            }
            \caption{Convergence en séquentiel avec redémarrage aléatoire}
        \end{figure}

        On observe que de nombreuses solutions
        ont une erreur inférieure à $10^{-3}$,
        et on trouve des solutions meilleures que la première.
        C'est donc une nette amélioration.

    \section{Conclusion}

        Dans tous les problèmes étudiés, on trouve des paires de Ritz
        satisfaisantes, mais le schéma de convergence est chaotique.
        On n'a dans aucun cas un comportement de convergence
        décroissant en erreur, même très grossièrement, de la méthode.

        Nos techniques, qui jouent sur le redémarrage et de la recherche
        en parallèle, augmentent la probabilité de diriger l'exploration
        vers une bonne solution sans se ``perdre''.
        Nous aimerions tout de même avoir un comportement reproductible
        et une convergence controlée.

\bibliographystyle{ieeetr}
\bibliography{biblio}

\end{document}

